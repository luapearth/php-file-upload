$(function() {

	var validTypes = ['image/jpeg', 'image/png', 'application/pdf'],
			files = [],
			fileNames = []
			formData = new FormData();
	$('#file').on('change', function(){
	    files = [];
	    if (window.File && window.FileReader && window.FileList && window.Blob){
	        var self = $(this)[0].files
	        if ((fileNames.length + self.length) <= 3){
	            $.each(self, function(k, v){
	                if (v.size <= 1024000){
	                    if(validTypes.indexOf(v.type) !== -1){
	                        v.showed = false;
	                        files.push(v);
	                        
	                        if (fileNames.indexOf(v.name) === -1){
	                            if ( window.FileReader ) {
									reader = new FileReader();
									reader.onloadend = function (e) { 
									    $(".file-preview").append('<li class="list-group-item image"><div class="image-prev-container"><img src='+e.target.result+' height="150"><div class="image-prev-name"><span>'+e.target.name+'</span></div><div class="delete-button"><i class="fa fa-times"></i><div></li>');
									};
								  	reader.readAsDataURL(v);
								}
								if (formData) {
								  	formData.append("images[]", v);
								}
	                            
	                            fileNames.push(v.name);
	                        }
	                    }
	                } else {
	                    alert('File size too large!');
	                }
	            });
	        } else {
	            alert('You can only upload 3 files.');
	        }
	    }
	});

	$('#upload').on('click', function() {
		$.ajax({
			url: 'upload.php',
			type: 'POST',
			data: formData,
		  	processData: false,  // tell jQuery not to process the data
		  	contentType: false,   // tell jQuery not to set contentType
		  	success: function(resp) {
		  		console.log(resp);
		  	}
		})
	});

});