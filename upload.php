<?php

$fileUploaded = 0;
$uploads_dir = 'uploads';
foreach ($_FILES["images"]["error"] as $key => $error) {
    if ($error == UPLOAD_ERR_OK) {
        $tmp_name = $_FILES["images"]["tmp_name"][$key];
        $name = $_FILES["images"]["name"][$key];
        move_uploaded_file($tmp_name, "$uploads_dir/$name");
        $fileUploaded++;
    }
}
if (count($_FILES) >= 1 && $fileUploaded >= 1) {
	echo "$fileUploaded file(s) uploaded";
} else {
	echo "nothing to upload!";
}
